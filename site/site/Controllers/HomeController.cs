﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace site.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }
       public ActionResult About()
        {
            ViewBag.Message = "your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "your contact page.";
            return View();
        }
       

    }
}

       