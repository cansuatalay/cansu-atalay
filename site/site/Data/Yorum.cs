﻿using System.ComponentModel.DataAnnotations;

namespace site.Data
{
    public class Yorum
    {
        public int YorumId { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        public string Icerik { get; set; }

        public virtual Makale Makale { get; set; }

        //Her yorumu, yalnızca bir kişi yazmış olabilir.
        public virtual Uye Uye { get; set; }
    }
}